#-------------------------------------------------
#
# Project created by QtCreator 2016-07-23T16:19:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = M2Mod
TEMPLATE = app


SOURCES += main.cpp\
        m2mod.cpp \
    ../M2Lib/DataBinary.cpp \
    ../M2Lib/M2.cpp \
    ../M2Lib/M2Element.cpp \
    ../M2Lib/M2Skin.cpp \
    ../M2Lib/M2SkinBuilder.cpp \
    ../M2Lib/M2Types.cpp

HEADERS  += m2mod.h \
    ../M2Lib/BaseTypes.h \
    ../M2Lib/DataBinary.h \
    ../M2Lib/M2.h \
    ../M2Lib/M2Element.h \
    ../M2Lib/M2I.h \
    ../M2Lib/M2Skin.h \
    ../M2Lib/M2SkinBuilder.h \
    ../M2Lib/M2Types.h

FORMS    += m2mod.ui

RC_FILE = m2mod.rc

