#ifndef M2MOD_H
#define M2MOD_H

#include <QMainWindow>

namespace Ui {
class M2Mod;
}

class M2Mod : public QMainWindow
{
    Q_OBJECT

public:
    explicit M2Mod(QWidget *parent = 0);
    ~M2Mod();

private slots:
    void on_buttonGo_clicked();

    void on_buttonInputM2ExpBrowse_clicked();

    void on_buttonOutputM2IBrowse_clicked();

    void on_buttonInputM2Browse_clicked();

    void on_buttonInputM2IBrowse_clicked();

    void on_buttonOutputM2Browse_clicked();

private:
    Ui::M2Mod *ui;
};

#endif // M2MOD_H
