#include "m2mod.h"
#include "ui_m2mod.h"

#include <QFileDialog>
#include <../M2Lib/M2.h>

#define M2Filter "M2 Files (*.m2);;All Files (*.*)"
#define M2IFilter "M2I Files (*.m2i);;All Files (*.*)"

M2Mod::M2Mod(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::M2Mod)
{
    ui->setupUi(this);
/*
    ui->textBoxInputM2Exp->setText("D:/World of Warcraft/Character/Goblin/Female/GoblinFemale.m2");
    ui->textBoxOutputM2I->setText("D:/World of Warcraft/Character/Goblin/Female/GoblinFemale2.m2i");

    ui->textBoxInputM2->setText("D:/World of Warcraft/Character/Human_orig/Female/HumanFemale_HD.m2");
    ui->textBoxInputM2I->setText("D:/World of Warcraft/Character/Human/Female/HumanFemale_HD.m2i");
    ui->textBoxOutputM2->setText("D:/World of Warcraft/Character/Human/Female/HumanFemale_HD.m2");
*/
}

M2Mod::~M2Mod()
{
    delete ui;
}

void M2Mod::on_buttonGo_clicked()
{
    ui->buttonGo->setEnabled(false);
    ui->buttonGo->repaint();
    ui->labelStatus->setText("Working...");
    ui->labelStatus->repaint();

    M2Lib::M2 M2;

    // Export mode.
    if (ui->tabExport->isVisible())
    {
        // Check fields.
        if (ui->textBoxInputM2Exp->text().isEmpty())
        {
            ui->labelStatus->setText("Error: No input M2 file Specified.");
            ui->buttonGo->setEnabled(true);
            return;
        }

        if (ui->textBoxOutputM2I->text().isEmpty())
        {
            ui->labelStatus->setText("Error: No output M2I file Specified.");
            ui->buttonGo->setEnabled(true);
            return;
        }

        // import M2
        M2Lib::EError Error = M2.Load((const Char16*)(ui->textBoxInputM2Exp->text().constData()));

        if (Error != 0)
        {
            ui->labelStatus->setText(M2Lib::GetErrorText(Error));
            ui->buttonGo->setEnabled(true);
            return;
        }

        // export M2I
        Error = M2.ExportM2Intermediate((Char16*)(ui->textBoxOutputM2I->text().data()));

        if (Error != 0)
        {
            ui->labelStatus->setText(M2Lib::GetErrorText(Error));
            ui->buttonGo->setEnabled(true);
            return;
        }

        ui->labelStatus->setText("Export done.");
    }
    // Import mode.
    if (ui->tabImport->isVisible())
    {
        // Check fields.
        if (ui->textBoxInputM2I->text().isEmpty())
        {
            ui->labelStatus->setText("Error: No input M2I file Specified.");
            ui->buttonGo->setEnabled(true);
            return;
        }

        if (ui->textBoxOutputM2->text().isEmpty())
        {
            ui->labelStatus->setText("Error: No output M2 file Specified.");
            ui->buttonGo->setEnabled(true);
            return;
        }

        // import M2
        if (ui->textBoxInputM2->text().isEmpty())
        {
            ui->labelStatus->setText("Error: No input M2 file Specified.");
            ui->buttonGo->setEnabled(true);
            return;
        }

        // import M2
        M2Lib::EError Error = M2.Load((const Char16*)(ui->textBoxInputM2->text().constData()));

        if (Error != 0)
        {
            ui->labelStatus->setText(M2Lib::GetErrorText(Error));
            ui->buttonGo->setEnabled(true);
            return;
        }

        // import M2I
        Error = M2.ImportM2Intermediate((Char16*)(ui->textBoxInputM2I->text().data()),
                                        !ui->checkBoxMergeBones->isChecked(),
                                        !ui->checkBoxMergeAttachments->isChecked(),
                                        !ui->checkBoxMergeCameras->isChecked());

        if (Error != 0)
        {
            ui->labelStatus->setText(M2Lib::GetErrorText(Error));
            ui->buttonGo->setEnabled(true);
            return;
        }

        // export M2
        Error = M2.Save((Char16*)(ui->textBoxOutputM2->text().data()), ui->checkBoxLegion->isChecked());

        if (Error != 0)
        {
            ui->labelStatus->setText(M2Lib::GetErrorText(Error));
            ui->buttonGo->setEnabled(true);
            return;
        }

        ui->labelStatus->setText("Import done.");
    }

    ui->buttonGo->setEnabled(true);
}

void M2Mod::on_buttonInputM2ExpBrowse_clicked()
{
    QFileDialog fileDialog(this);
    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.setNameFilter(tr(M2Filter));
    fileDialog.setFileMode(QFileDialog::ExistingFile);

    if (fileDialog.exec())
    {
        ui->textBoxInputM2Exp->setText(fileDialog.selectedFiles().first());
        ui->textBoxOutputM2I->setText(fileDialog.selectedFiles().first().append("i"));
    }
}

void M2Mod::on_buttonOutputM2IBrowse_clicked()
{
    QFileDialog fileDialog(this);
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setNameFilter(tr(M2IFilter));
    fileDialog.setFileMode(QFileDialog::AnyFile);

    if (fileDialog.exec())
    {
        ui->textBoxOutputM2I->setText(fileDialog.selectedFiles().first());
    }
}


void M2Mod::on_buttonInputM2Browse_clicked()
{
    QFileDialog fileDialog(this);
    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.setNameFilter(tr(M2Filter));
    fileDialog.setFileMode(QFileDialog::ExistingFile);

    if (fileDialog.exec())
    {
        ui->textBoxInputM2->setText(fileDialog.selectedFiles().first());
        ui->textBoxInputM2I->setText(fileDialog.selectedFiles().first().append("i"));
        ui->textBoxOutputM2->setText(fileDialog.selectedFiles().first());
    }
}

void M2Mod::on_buttonInputM2IBrowse_clicked()
{
    QFileDialog fileDialog(this);
    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.setNameFilter(tr(M2IFilter));
    fileDialog.setFileMode(QFileDialog::ExistingFile);

    if (fileDialog.exec())
    {
        ui->textBoxInputM2I->setText(fileDialog.selectedFiles().first());
    }
}

void M2Mod::on_buttonOutputM2Browse_clicked()
{
    QFileDialog fileDialog(this);
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setNameFilter(tr(M2Filter));
    fileDialog.setFileMode(QFileDialog::AnyFile);

    if (fileDialog.exec())
    {
        ui->textBoxOutputM2->setText(fileDialog.selectedFiles().first());
    }
}
