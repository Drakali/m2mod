#include "M2Element.h"
#include <assert.h>
#include <string.h>


UInt32 M2Lib::M2Element::FileOffset = 0;

M2Lib::M2Element::M2Element()
	: Count(0)
	, Offset(0)
	, DataSize(0)
	, Data(0)
	, Align(16)
{
}

M2Lib::M2Element::~M2Element()
{
	if (Data)
	{
		delete[] Data;
	}
}

void* M2Lib::M2Element::GetLocalPointer(UInt32 GlobalOffset)
{
	assert(GlobalOffset >= Offset);
	GlobalOffset -= Offset;
	assert(GlobalOffset < (UInt32)DataSize);
	return &Data[GlobalOffset];
}

bool M2Lib::M2Element::Load(std::fstream& FileStream)
{
	if (!DataSize)
		return true;

	Data = new UInt8[DataSize];
    FileStream.seekg(Offset + FileOffset, std::ios::beg);
	FileStream.read((Char8*)Data, DataSize);

	return true;
}

bool M2Lib::M2Element::Save(std::fstream& FileStream)
{
	if (!DataSize)
		return true;

    FileStream.seekp(Offset + FileOffset);
	FileStream.write((Char8*)Data, DataSize);

	return true;
}

void M2Lib::M2Element::SetDataSize(UInt32 NewCount, UInt32 NewDataSize, bool CopyOldData)
{
	if (NewDataSize <= (UInt32)DataSize)
		return;

	if (Align != 0)
	{
		UInt32 Mod = NewDataSize % Align;
		if (Mod)
		{
			NewDataSize += Align - Mod;
		}
	}

	UInt8* NewData = new UInt8[NewDataSize];
	if (Data)
	{
		if (CopyOldData)
		{
			memset(&NewData[DataSize], 0, NewDataSize - DataSize);
			memcpy(NewData, Data, DataSize);
		}
		else
		{
			memset(NewData, 0, NewDataSize);
		}
		delete[] Data;
	}
    else
    {
        memset(NewData, 0, NewDataSize);
    }

	Data = NewData;
	DataSize = NewDataSize;
	Count = NewCount;
}

void M2Lib::M2Element::Clone(M2Element* Source, M2Element* Destination)
{
	Destination->SetDataSize(Source->Count, Source->DataSize, false);
	memcpy(Destination->Data, Source->Data, Source->DataSize);
}

void M2Lib::M2Element::SetFileOffset(UInt32 offset)
{
    FileOffset = offset;
}

int M2Lib::OpenStream(std::fstream& FileStream, const Char16 * FileName, std::ios_base::openmode Mode)
{
#ifdef __MINGW_MBWC_CONVERT_DEFINED
        char * UTF8FileName;
        __mingw_str_wide_utf8(FileName, &UTF8FileName, 0);

        int ErrorCode;
        FileStream.open(UTF8FileName, Mode);
        __mingw_str_free(UTF8FileName);

        return ErrorCode;
#else
        return FileStream.open(FileName, Mode);
#endif
}
